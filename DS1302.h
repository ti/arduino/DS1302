uint8_t decodeBcd(uint8_t value) {
    return ((value & 0xF0) >> 4) * 10 + (value & 0x0F);
}

uint8_t encodeBcd(uint8_t value) {
    return ((value / 10) << 4) | (value % 10);
}

class DS1302 {
    uint8_t pinLoad;
    uint8_t pinData;
    uint8_t pinClock;

    void serializeByte(uint8_t value) const {
        pinMode(pinData, OUTPUT);
        for(uint8_t i = 1; i != 0; i <<= 1) {
            digitalWrite(pinData, (value & i) != 0);
            //delay(1);
            digitalWrite(pinClock, HIGH);
            //delay(1);
            digitalWrite(pinClock, LOW);
            //delay(1);
        }
    }

    uint8_t deserializeByte() const {
        uint8_t value = 0;

        pinMode(pinData, INPUT);
        for(uint8_t i = 0; i < 8; ++i) {
            value |= digitalRead(pinData) << i;
            //delay(1);
            digitalWrite(pinClock, HIGH);
            //delay(1);
            digitalWrite(pinClock, LOW);
            //delay(1);
        }

        return value;
    }

public:
    DS1302(uint8_t pinLoad, uint8_t pinData, uint8_t pinClock)
        : pinLoad(pinLoad), pinData(pinData), pinClock(pinClock) {
        pinMode(pinClock, OUTPUT);
        pinMode(pinLoad, OUTPUT);
        digitalWrite(pinClock, LOW);
        digitalWrite(pinLoad, LOW);
    }

    // Only for writing, +1 for reading
    enum Unit : uint8_t {
        SECOND = 0x80,
        MINUTE = 0x82,
        HOUR = 0x84,
        DAY = 0x86,
        MONTH = 0x88,
        WEEKDAY = 0x8a,
        YEAR = 0x8c,
    };

    uint8_t read(Unit unit) const {
        uint8_t mask = 0xFF;
        if(unit == Unit::HOUR || unit == Unit::SECOND) {
            // ignore the first bit
            mask = 0x7F;
        }

        return decodeBcd(mask & readByte(unit + 1));
    }

    void write(Unit unit, uint8_t value) const {
        writeByte(unit, encodeBcd(value));
    }

    void setWriteProtect(bool active) {
        if(active) {
            writeByte(0x8e, 0x80);
        } else {
            writeByte(0x8e, 0x00);
        }
    }

    uint8_t readByte(uint8_t address) const {
        digitalWrite(pinLoad, HIGH);

        serializeByte(address);
        auto value = deserializeByte();

        digitalWrite(pinLoad, LOW);

        return value;
    }

    void writeByte(uint8_t address, uint8_t value) const {
        digitalWrite(pinLoad, HIGH);

        serializeByte(address);
        serializeByte(value);

        digitalWrite(pinLoad, LOW);
    }
};

class DateTime {
    char formatBuffer[20];

public:
    uint8_t year = 0;
    uint8_t month = 0;
    uint8_t day = 0;
    uint8_t hour = 0;
    uint8_t minute = 0;
    uint8_t second = 0;
    uint8_t weekday = 0;

    const char* format() {
        snprintf(formatBuffer, sizeof(formatBuffer),
            "%04d-%02d-%02d %02d:%02d:%02d",
            2000 + year,
            month,
            day,
            hour,
            minute,
            second
        );

        return formatBuffer;
    }

    const char* formatWeekday() const {
        switch(weekday) {
            case 1: return "Mon";
            case 2: return "Tue";
            case 3: return "Wed";
            case 4: return "Thu";
            case 5: return "Fri";
            case 6: return "Sat";
            case 7: return "Sun";
            default: return "Err";
        }
    }

    template<typename RTC>
    void readFromRtc(RTC& rtc) {
        year = rtc.read(RTC::YEAR);
        weekday = rtc.read(RTC::WEEKDAY);
        month = rtc.read(RTC::MONTH);
        day = rtc.read(RTC::DAY);
        hour = rtc.read(RTC::HOUR);
        minute = rtc.read(RTC::MINUTE);
        second = rtc.read(RTC::SECOND);
    }

    template<typename RTC>
    void writeToRtc(RTC& rtc) const {
        rtc.setWriteProtect(false);

        rtc.write(RTC::YEAR, year);
        rtc.write(RTC::WEEKDAY, weekday);
        rtc.write(RTC::MONTH, month);
        rtc.write(RTC::DAY, day);
        rtc.write(RTC::HOUR, hour);
        rtc.write(RTC::MINUTE, minute);
        rtc.write(RTC::SECOND, second);

        // write protect enable
        rtc.setWriteProtect(true);
    }

    void tick() {
        second =  (second + 1) % 60;
        if(second == 0)  {
            minute = (minute + 1) % 60;
            if(minute == 0) {
                hour = (hour + 1) % 24;
                // TODO
            }
        }
    }
};

